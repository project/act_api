CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

This module is for connecting via OAuth to the act! API.
That is all this does, but it gives you a method and a class to work
with to call the API to get results..

 * For a full description on how to use the API portion of the module, visit
   the project page: https://drupal.org/project/act_api


REQUIREMENTS
------------

This module requires https://drupal.org/projectcomposer_manager and
https://drupal.org/projectxautoload (>= 7.x-5.0).


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

    - Administer act! API Settings

     Allows users to see and use the Blackbaud SKY API config setting page
     located at: /admin/config/services/act

 * Go to Administration » Configuration » act! API Settings.

    - Add in your url of act! api website, act! web api username,
      act! web api password & act! web api database name.
