<?php

/**
 * @file
 * Admin File for act! API.
 */

use Drupal\act_api\ActApi;

/**
 * The act! API Form.
 */
function act_api_settings_form($form, &$form_state) {

  // Application fieldset.
  $form['app'] = array(
    '#type' => 'fieldset',
    '#title' => t('Application Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  // Descriptive text.
  $tdesc = t('You will need to have act! Premium Web v18 or greater + the web api installed.  If using v20 or greater, the web api is installed automatically.');
  $form['app']['title'] = array(
    '#type' => 'item',
    '#markup' => '<span><strong>' . $tdesc . '</strong></span>',
  );

  // Base URL.
  $form['app']['act_api_base_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL of act! API website'),
    '#default_value' => variable_get('act_api_base_url', ''),
    '#required' => TRUE,
  );

  // The Username.
  $form['app']['act_api_user_name'] = array(
    '#type' => 'textfield',
    '#title' => t('act! Web Username'),
    '#default_value' => variable_get('act_api_user_name', ''),
    '#required' => TRUE,
  );

  // The Password.
  $form['app']['act_api_password'] = array(
    '#type' => 'textfield',
    '#title' => t('act! Web Password'),
    '#default_value' => variable_get('act_api_password', ''),
    '#required' => TRUE,
  );

  // The Password.
  $form['app']['act_api_database'] = array(
    '#type' => 'textfield',
    '#title' => t('act! Web Database Name'),
    '#default_value' => variable_get('act_api_database', ''),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Blackbaud Authorize Form.
 */
function act_api_authorize_form($form, &$form_state) {
  // Check both methods to make sure we have auth.
  $token = variable_get('act_api_access_token', '');

  // If we have a token.
  if (!empty($token)) {
    $bb = new ActApi();
    $api = $bb->checkToken();
  }

  // Show the submit or not.
  if (empty($token) || !$api) {
    // We are not Authorized, instruct the people.
    $form['title'] = array(
      '#type' => 'item',
      '#markup' => '<center><h3>act! API is not authorized on this site.  Click Authorize below and follow the prompts.<h3></center>',
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Authorize'),
      '#prefix' => '<center>',
      '#suffix' => '</center>',
    );
  }
  else {
    // We are good!
    $form['title'] = array(
      '#type' => 'item',
      '#markup' => '<center><h3>act! API is authorized, you may carry on.<h3></center>',
    );
  }

  return $form;
}

/**
 * Blackbaud Authorize Form.
 */
function act_api_authorize_form_validate($form, &$form_state) {
  // Instantiate the BlackBaud request and grab the code.
  $bb = new ActApi();
  $bb->getAuthCode();
}
