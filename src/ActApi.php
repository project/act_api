<?php

namespace Drupal\act_api;

use Drupal\act_api\Act;

/**
 * Class ActApi.
 *
 * @package Drupal\act_api
 */
class ActApi extends Act {

  /**
   * Mechanism to call the API.
   *
   * @param string $endpoint
   *   The endpoint url without the base url.
   * @param array $add_options
   *   Additional options to pass into the API call.
   * @param string $method
   *   The type of request (ie GET, POST, etc).
   *
   * @return null|object
   *   The API response or NULL.
   */
  public function callApi($endpoint, array $add_options = [], $method = 'GET') {
    $token = parent::getToken('access');
    $options = array(
      'headers' => array(
        'Accept' => 'application/json',
        'Authorization' => 'Bearer ' . $token,
      ),
    );

    // Merge in any additional options.
    $options = array_merge($options, $add_options);

    parent::setUrl($this->getApiBaseUrl() . $endpoint);
    parent::requestResponse($method, $options);

    // Exit if Empty Response.
    if ($this->request === NULL) {
      return NULL;
    }

    // Grab the Body and return as needed.
    $contents = json_decode($this->request->getBody()->getContents());
    return !empty($contents) ? $contents : NULL;
  }

  /**
   * Pings a simple api call to test if the token is valid.
   *
   * @return bool
   *   If we are able to ping the API or not.
   */
  public function checkToken() {
    $check = $this->callApi('/api/users');
    return $check === NULL ? FALSE : TRUE;
  }

}
