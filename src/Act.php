<?php

namespace Drupal\act_api;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;

/**
 * Class Act.
 *
 * @package Drupal\act_api
 */
abstract class Act {

  /**
   * The url we are checking.
   *
   * @var string
   */
  protected $url;

  /**
   * The Request Response.
   *
   * @var GuzzleHttp\Client
   */
  protected $request;

  /**
   * Get the Oauth base URL we are checking.
   *
   * @return string
   *   The OAuth base URL.
   */
  protected function getApiBaseUrl() {
    return variable_get('act_api_base_url', '') . '/act.web.api';
  }

  /**
   * Set the url we are checking.
   *
   * @param string $url
   *   The url string we are checking.
   */
  protected function setUrl($url) {
    $this->url = $url;
  }

  /**
   * Get the url we are checking.
   *
   * @return string
   *   The url string we are checking.
   */
  protected function getUrl() {
    return $this->url;
  }

  /**
   * Set the url we are checking.
   *
   * @param string $type
   *   The type of token we are setting.
   * @param string $token
   *   The token we are setting.
   */
  protected function setToken($type, $token) {
    variable_set('act_api_' . $type . '_token', $token);
  }

  /**
   * Get the url we are checking.
   *
   * @return string
   *   The type of token we are getting.
   */
  protected function getToken($type) {
    return variable_get('act_api_' . $type . '_token', '');
  }

  /**
   * Get the Authorization code.
   */
  public function getAuthCode() {
    // Access Token Url.
    $this->setUrl($this->getApiBaseUrl() . '/authorize');

    // Grab these for the auth header.
    $username = variable_get('act_api_user_name', '');
    $password = variable_get('act_api_password', '');
    $db = variable_get('act_api_database', '');

    // Set header for this post request.
    $header = array(
      'Content-type' => 'application/json',
      'Authorization' => 'Basic ' . base64_encode($username . ':' . $password),
      'Act-Database-Name' => $db,
    );

    // Set the options.
    $options = array(
      'headers' => $header,
    );

    // Post the Data.
    $this->requestResponse('POST', $options);

    // Set the access token.
    $this->setToken('access', $this->request->getBody()->getContents());

    // Make sure we only redirect on UI auth request.
    if (isset($_GET['q']) && $_GET['q'] === 'admin/config/services/act/authorize') {
      drupal_goto('admin/config/services/act/authorize');
    }
  }

  /**
   * Gets the response of a page.
   *
   * @param string $type
   *   The type of request (ie GET, POST, etc).
   * @param array $options
   *   Any options to pass in.
   */
  protected function requestResponse($type, array $options = array()) {
    $client = new Client();

    // Set the options for the request.
    // @see http://docs.guzzlephp.org/en/latest/request-options.html
    $options += array(
      'http_errors' => FALSE,
      'synchronous' => TRUE,
    );

    try {
      // Try the request.
      $response = $client->request($type, $this->getUrl(), $options);

      // Check the Status code and return.
      switch ($response->getStatusCode()) {
        // All good, send back response.
        case '200':
          $this->request = $response;
          break;

        // Unauthorized, try again.
        case '401':
          $this->getAuthCode();
          break;

        // Something else is amiss.
        default:
          $message = 'The request to the act! API resulted in a ' . $response->getStatusCode() . ' Response.';
          watchdog('act! API', $message);
          $this->request = NULL;
          break;
      }
    }
    catch (TransferException $e) {
      $this->request = NULL;
    }
  }

}
